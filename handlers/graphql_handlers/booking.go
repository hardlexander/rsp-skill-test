package graphql_handlers

import (
	"context"
	
	"RSP-skill-test/modules/booking"
)

type BookingHandler interface {
	CreateBookingHandler(ctx context.Context, req *booking.CreateBookingRequest) error
	CheckInHandler(ctx context.Context, id int64) error
	CheckOutHandler(ctx context.Context, id int64) error

}

type bookingHandler struct {
	service booking.Service
	
}

func (b bookingHandler) CreateBookingHandler(ctx context.Context, req *booking.CreateBookingRequest) error {
	return b.service.CreateBooking(req)
}

func (b bookingHandler) CheckInHandler(ctx context.Context, id int64) error {
	return b.service.CheckIn(id)
}

func (b bookingHandler) CheckOutHandler(ctx context.Context, id int64) error {
	return b.service.CheckOut(id)
}

func InitBookingHandler(service booking.Service) BookingHandler {
	return &bookingHandler{service}
}
