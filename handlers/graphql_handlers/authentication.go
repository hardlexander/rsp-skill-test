package graphql_handlers

import (
	"context"
	
	"RSP-skill-test/modules/authentication"
)

type AuthenticationHandler interface {
	GuestRegisterHandler(ctx context.Context, email, password string) error
}

type authenticationHandler struct {
	service authentication.Service
}

func (a authenticationHandler) GuestRegisterHandler(ctx context.Context, email, password string) error {
	req := authentication.GuestRegisterRequest{
		Email:    email,
		Password: password,
	}
	
	err := a.service.GuestRegister(req)
	return err
}

func InitAuthenticationHandler(service authentication.Service) AuthenticationHandler {
	return &authenticationHandler{service}
}
