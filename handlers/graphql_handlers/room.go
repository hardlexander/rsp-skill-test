package graphql_handlers

import (
	"context"
	
	"RSP-skill-test/modules/room"
)

type RoomHandler interface {
	GetAvailableRoomsHandler(ctx context.Context, date string) (*room.GetAvailableRoomsResponse, error)
	CreateRoomHandler(ctx context.Context, req *room.CreateRoomRequest) error
}

type roomHandler struct {
	service room.Service
}

func (r roomHandler) CreateRoomHandler(ctx context.Context, req *room.CreateRoomRequest) error {
	err := r.service.CreateRoom(req)
	return err
}

func (r roomHandler) GetAvailableRoomsHandler(ctx context.Context, date string) (*room.GetAvailableRoomsResponse, error) {
	req := &room.GetAvailableRoomsRequest{Date: date}
	
	rooms, err := r.service.GetAvailableRooms(req)
	return rooms, err
}

func InitRoomHandler(service room.Service) RoomHandler {
	return &roomHandler{service}
}
