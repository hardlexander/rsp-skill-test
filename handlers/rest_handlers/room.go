package rest_handlers

import (
	"fmt"
	"net/http"
	
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"github.com/labstack/gommon/log"
	
	"RSP-skill-test/modules/room"
	"RSP-skill-test/pkg/response"
)

type RoomHandler interface {
	CreateRoomHandler(c echo.Context) error
	GetAvailableRoomsHandler(c echo.Context) error

}

type roomHandler struct {
	service room.Service
}

func (r roomHandler) GetAvailableRoomsHandler(c echo.Context) error {
	req := &room.GetAvailableRoomsRequest{Date: c.QueryParam("date")}
	resp := new(response.BaseResponse)
	
	rooms, err := r.service.GetAvailableRooms(req)
	if err != nil {
		resp.Success = false
		resp.Message = err.Error()
		return c.JSON(http.StatusUnauthorized, resp)
	}
	
	resp.Success = true
	resp.Message = fmt.Sprintf("success to retrieve available rooms on %s", req.Date)
	resp.Data = rooms
	return c.JSON(http.StatusOK, resp)
}

func (r roomHandler) CreateRoomHandler(c echo.Context) error {
	req := new(room.CreateRoomRequest)
	resp := new(response.BaseResponse)
	
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	role := claims["role"].(string)
	
	if role != "admin" {
		resp.Success = false
		resp.Message = "unauthorized"
		return c.JSON(http.StatusUnauthorized, resp)
	}
	
	// get photo file
	photo, err := c.FormFile("photo")
	if err != nil {
		resp.Success = false
		resp.Message = err.Error()
		return c.JSON(http.StatusInternalServerError, resp)
	}
	req.Photo = photo
	req.RoomName = c.FormValue("room_name")
	req.RoomCapacity = c.FormValue("room_capacity")
	
	if err := r.service.CreateRoom(req); err != nil {
		resp.Success = false
		resp.Message = err.Error()
		log.Error(err)
		return c.JSON(http.StatusBadRequest, resp)
	}
	
	resp.Success = true
	resp.Message = "success to create a new room"
	return c.JSON(http.StatusCreated, resp)
}

func InitRoomHandler(service room.Service) RoomHandler {
	return &roomHandler{service}
}
