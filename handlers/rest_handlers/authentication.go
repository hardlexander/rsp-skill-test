package rest_handlers

import (
	"net/http"
	
	"github.com/labstack/echo"
	"github.com/labstack/gommon/log"
	
	"RSP-skill-test/modules/authentication"
	"RSP-skill-test/pkg/response"
)

type AuthenticationHandler interface {
	AdminLoginHandler(c echo.Context) error
	GuestRegisterHandler(c echo.Context) error
	GuestLoginHandler(c echo.Context) error

}

type authenticationHandler struct {
	service authentication.Service
	
}

func (h authenticationHandler) AdminLoginHandler(c echo.Context) error {
	resp := new(response.BaseResponse)
	
	// get username and password from auth header
	username, password, ok := c.Request().BasicAuth()
	if !ok {
		resp.Success = false
		resp.Message = "authentication failed"
		return c.JSON(http.StatusBadRequest, resp)
	}
	
	req := authentication.AdminLoginRequest{
		Username: username,
		Password: password,
	}
	
	token, err := h.service.AdminLogin(req)
	if err != nil {
		resp.Success = false
		resp.Message = err.Error()
		log.Error(err)
		return c.JSON(http.StatusBadRequest, resp)
	}
	
	resp.Success = true
	resp.Message = "login success"
	resp.Data = token
	return c.JSON(http.StatusOK, resp)
}

func (h authenticationHandler) GuestRegisterHandler(c echo.Context) error {
	resp := new(response.BaseResponse)
	username, password, ok := c.Request().BasicAuth()
	if !ok {
		resp.Success = false
		resp.Message = "authentication failed"
		return c.JSON(http.StatusBadRequest, resp)
	}
	
	// get photo file
	photo, err := c.FormFile("photo")
	if err != nil {
		resp.Success = false
		resp.Message = err.Error()
		return c.JSON(http.StatusInternalServerError, resp)
	}
	
	req := authentication.GuestRegisterRequest{
		Email:    username,
		Password: password,
		Photo:    photo,
	}
	
	if err := h.service.GuestRegister(req); err != nil {
		resp.Success = false
		resp.Message = err.Error()
		log.Error(err)
		return c.JSON(http.StatusBadRequest, resp)
	}
	
	resp.Success = true
	resp.Message = "registration success"
	return c.JSON(http.StatusOK, resp)
}

func (h authenticationHandler) GuestLoginHandler(c echo.Context) error {
	resp := new(response.BaseResponse)
	username, password, ok := c.Request().BasicAuth()
	if !ok {
		resp.Success = false
		resp.Message = "authentication failed"
		return c.JSON(http.StatusBadRequest, resp)
	}
	
	req := authentication.GuestLoginRequest{
		Email:    username,
		Password: password,
	}
	
	token, err := h.service.GuestLogin(req)
	if err != nil {
		resp.Success = false
		resp.Message = err.Error()
		log.Error(err)
		return c.JSON(http.StatusBadRequest, resp)
	}
	
	resp.Success = true
	resp.Message = "login success"
	resp.Data = token
	return c.JSON(http.StatusOK, resp)
}

func InitAuthenticationHandler(service authentication.Service) AuthenticationHandler {
	return &authenticationHandler{service}
}