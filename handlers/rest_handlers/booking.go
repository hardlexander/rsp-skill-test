package rest_handlers

import (
	"net/http"
	"strconv"
	
	"github.com/labstack/echo"
	"github.com/labstack/gommon/log"
	
	"RSP-skill-test/modules/booking"
	"RSP-skill-test/pkg/response"
)

type BookingHandler interface {
	CreateBookingHandler(c echo.Context) error
	CheckInHandler(c echo.Context) error
	CheckOutHandler(c echo.Context) error
}

type bookingHandler struct {
	service booking.Service
	
}

func (b bookingHandler) CreateBookingHandler(c echo.Context) error {
	req := new(booking.CreateBookingRequest)
	resp := new(response.BaseResponse)
	
	if err := c.Bind(req); err != nil {
		resp.Success = false
		resp.Message = err.Error()
		return c.JSON(http.StatusBadRequest, resp)
	}
	
	if err := b.service.CreateBooking(req); err != nil {
		resp.Success = false
		resp.Message = err.Error()
		log.Error(err)
		return c.JSON(http.StatusBadRequest, resp)
	}
	
	resp.Success = true
	resp.Message = "success to book a room"
	return c.JSON(http.StatusCreated, resp)
	
}

func (b bookingHandler) CheckInHandler(c echo.Context) error {
	resp := new(response.BaseResponse)
	idString := c.Param("id")
	id, err := strconv.ParseInt(idString, 10, 64)
	if err != nil {
		resp.Success = false
		resp.Message = err.Error()
		log.Error(err)
		return c.JSON(http.StatusBadRequest, resp)
	}
	
	if err := b.service.CheckIn(id); err != nil {
		resp.Success = false
		resp.Message = err.Error()
		log.Error(err)
		return c.JSON(http.StatusBadRequest, resp)
	}
	
	resp.Success = true
	resp.Message = "success to check in"
	return c.JSON(http.StatusOK, resp)
}

func (b bookingHandler) CheckOutHandler(c echo.Context) error {
	resp := new(response.BaseResponse)
	idString := c.Param("id")
	id, err := strconv.ParseInt(idString, 10, 64)
	if err != nil {
		resp.Success = false
		resp.Message = err.Error()
		log.Error(err)
		return c.JSON(http.StatusBadRequest, resp)
	}
	
	if err := b.service.CheckOut(id); err != nil {
		resp.Success = false
		resp.Message = err.Error()
		log.Error(err)
		return c.JSON(http.StatusBadRequest, resp)
	}
	
	resp.Success = true
	resp.Message = "success to check checkout"
	return c.JSON(http.StatusOK, resp)
}

func InitBookingHandler(service booking.Service) BookingHandler {
	return &bookingHandler{service}
}

