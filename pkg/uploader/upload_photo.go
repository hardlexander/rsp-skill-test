package uploader

import (
	"errors"
	"fmt"
	"mime/multipart"
	"path/filepath"
	"strings"
	
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	
	"RSP-skill-test/config"
)

func UploadPhoto(file *multipart.FileHeader) (url string, err error) {
	// get file name and extension
	fileName := file.Filename
	extension := filepath.Ext(fileName)
	
	// make sure upload file is image in png, jpg, or jpeg format
	if extension != ".png" && extension != ".jpg" && extension != ".jpeg" {
		return "", errors.New("please upload image in one of these formats: .png, .jpg, .jpeg")
	}
	
	extension = strings.TrimPrefix(extension, ".")
	// open file
	photoFile, err := file.Open()
	if err != nil {
		return "", err
	}
	defer photoFile.Close()
	
	cfg := config.Get()
	
	// start an aws session
	sess := session.Must(session.NewSession(&aws.Config{
		Endpoint: &cfg.SpaceEndpoint,
		Region:   &cfg.SpaceRegion,
	}))
	
	// create an uploader with the session and default options
	uploader := s3manager.NewUploader(sess)
	
	// upload the file to S3 (Digital Ocean Space)
	result, err := uploader.Upload(&s3manager.UploadInput{
		Bucket:             aws.String(cfg.SpaceBucketName),
		Key:                aws.String(fileName),
		Body:               photoFile,
		ACL:                aws.String("public-read"),
		ContentType:        aws.String(fmt.Sprintf("image/%s", extension)),
		ContentDisposition: aws.String("inline"),
	})
	
	if err != nil {
		return "", err
	}
	
	// return the photo url
	return result.Location, nil
}
