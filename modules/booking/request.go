package booking

type CreateBookingRequest struct {
	RoomID       int64     `json:"room_id"`
	GuestID      int64     `json:"guest_id"`
	TotalPerson  int64     `json:"total_person"`
	BookingTime  string     `json:"booking_time"`
	Note         string    `json:"note"`
}
