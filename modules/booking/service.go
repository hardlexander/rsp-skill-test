package booking

import (
	"errors"
	"fmt"
	"time"
	
	"gopkg.in/gomail.v2"
	
	"RSP-skill-test/config"
	"RSP-skill-test/modules/authentication"
	"RSP-skill-test/modules/room"
)

type Service interface {
	CreateBooking(req *CreateBookingRequest) error
	CheckIn(id int64) error
	CheckOut(id int64) error
	NotifyTodayBookings() error
}

type service struct {
	repo Repository
	roomRepo room.Repository
	authRepo authentication.Repository
}

const (
	dateLayout = "02-01-2006"
)

var cfg config.Config

func init()  {
	cfg = config.Get()
}

func (s service) NotifyTodayBookings() error {
	bookings, err := s.repo.GetTodayBookings()
	if err != nil{
		return err
	}
	
	errs := make(chan error)
	for _, b := range bookings {
		go func(b Booking) {
			// get guest email
			guest, err := s.authRepo.GetGuestByID(b.GuestID)
			if err != nil{
				errs <- err
			}
			
			// get room name
			room, err := s.roomRepo.GetRoomByID(b.RoomID)
			if err != nil{
				errs <- err
			}
			
			mailer := gomail.NewMessage()
			mailer.SetHeader("From", cfg.EmailSenderName)
			mailer.SetHeader("To", guest.Email)
			mailer.SetHeader("Subject", "Your Booking is Today!")
			mailer.SetBody("text/plain", fmt.Sprintf("Your booking for room %s is today.", room.RoomName))
			
			dialer := gomail.NewDialer(
				cfg.SMTPHost,
				cfg.SMTPPort,
				cfg.AuthEmail,
				cfg.AuthPassword)
			
			err = dialer.DialAndSend(mailer)
			errs <- err
		}(b)
	}
	return <-errs
}

func (s service) CreateBooking(req *CreateBookingRequest) error {
	bookingTime, err := time.Parse(dateLayout, req.BookingTime)
	if err != nil {
		return err
	}
	
	roomIsBooked, err := s.repo.IsRoomBooked(req.RoomID, bookingTime)
	if err != nil {
		return err
	}
	
	if roomIsBooked {
		return errors.New("room is booked")
	}
	
	roomCapacity, err := s.roomRepo.GetRoomCapacity(req.RoomID)
	if err != nil {
		return err
	}
	
	if req.TotalPerson > roomCapacity {
		return errors.New("total person exceeds room capacity")
	}
	
	booking := &Booking{
		RoomID:       req.RoomID,
		GuestID:      req.GuestID,
		TotalPerson:  req.TotalPerson,
		BookingTime:  bookingTime,
		Note:         req.Note,
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
		DeletedAt:    nil,
	}
	
	err = s.repo.CreateBooking(booking)
	if err != nil{
		return err
	}
	
	// get guest email
	guest, err := s.authRepo.GetGuestByID(req.GuestID)
	if err != nil{
		return err
	}
	
	// get room name
	room, err := s.roomRepo.GetRoomByID(req.RoomID)
	if err != nil{
		return err
	}
	
	mailer := gomail.NewMessage()
	mailer.SetHeader("From", cfg.EmailSenderName)
	mailer.SetHeader("To", guest.Email)
	mailer.SetHeader("Subject", "Booking Notification")
	mailer.SetBody("text/plain", fmt.Sprintf("You have booked room %s for %s", room.RoomName, req.BookingTime))
	
	
	dialer := gomail.NewDialer(
		cfg.SMTPHost,
		cfg.SMTPPort,
		cfg.AuthEmail,
		cfg.AuthPassword)
	
	err = dialer.DialAndSend(mailer)
	return err
}

func (s service) CheckIn(id int64) error {
	booking, err := s.repo.CheckIn(id)
	if err != nil{
		return err
	}
	
	// get guest email
	guest, err := s.authRepo.GetGuestByID(booking.GuestID)
	if err != nil{
		return err
	}
	
	// get room name
	room, err := s.roomRepo.GetRoomByID(booking.RoomID)
	if err != nil{
		return err
	}
	
	mailer := gomail.NewMessage()
	mailer.SetHeader("From", cfg.EmailSenderName)
	mailer.SetHeader("To", guest.Email)
	mailer.SetHeader("Subject", "Check-in Notification")
	mailer.SetBody("text/plain", fmt.Sprintf("You have checked-in to room %s", room.RoomName))
	
	
	dialer := gomail.NewDialer(
		cfg.SMTPHost,
		cfg.SMTPPort,
		cfg.AuthEmail,
		cfg.AuthPassword)
	
	err = dialer.DialAndSend(mailer)
	return err
}

func (s service) CheckOut(id int64) error {
	booking, err := s.repo.CheckOut(id)
	if err != nil{
		return err
	}
	
	// get guest email
	guest, err := s.authRepo.GetGuestByID(booking.GuestID)
	if err != nil{
		return err
	}
	
	// get room name
	room, err := s.roomRepo.GetRoomByID(booking.RoomID)
	if err != nil{
		return err
	}
	
	mailer := gomail.NewMessage()
	mailer.SetHeader("From", cfg.EmailSenderName)
	mailer.SetHeader("To", guest.Email)
	mailer.SetHeader("Subject", "Check-out Notification")
	mailer.SetBody("text/plain", fmt.Sprintf("You have checked-out from room %s", room.RoomName))
	
	
	dialer := gomail.NewDialer(
		cfg.SMTPHost,
		cfg.SMTPPort,
		cfg.AuthEmail,
		cfg.AuthPassword)
	
	err = dialer.DialAndSend(mailer)
	return err
}

func InitService(repo Repository, roomRepo room.Repository, authRepo authentication.Repository) Service {
	return &service{repo, roomRepo, authRepo}
}
