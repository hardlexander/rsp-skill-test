package booking

import (
	"time"
	
	"gorm.io/gorm"
)

type Repository interface {
	CreateBooking(b *Booking) error
	CheckIn(bookingID int64) (*Booking, error)
	CheckOut(bookingID int64) (*Booking, error)
	IsRoomBooked(roomID int64, date time.Time) (bool, error)
	GetTodayBookings() ([]Booking, error)
}

type repository struct {
	db *gorm.DB
}

func (r repository) GetTodayBookings() ([]Booking, error) {
	bookingTimeDateLayout := "2006-01-02"
	todayDate := time.Now().Format(bookingTimeDateLayout)
	tomorrowDate := time.Now().Add(24 * time.Hour).Format(bookingTimeDateLayout)
	
	var bookings []Booking
	err := r.db.Model(&Booking{}).Where("booking_time >= ? AND booking_time < ?", todayDate,
		tomorrowDate).Find(&bookings).Error
	return bookings, err
}

func (r repository) CreateBooking(b *Booking) error {
	err := r.db.Create(b).Error
	return err
}

func (r repository) CheckIn(bookingID int64) (*Booking, error) {
	var b Booking
	err := r.db.Model(&Booking{}).Where("id", bookingID).Update("check_in_time", time.Now()).First(&b).Error
	return &b, err
}

func (r repository) CheckOut(bookingID int64) (*Booking, error) {
	var b Booking
	err := r.db.Model(&Booking{}).Where("id", bookingID).Update("check_out_time", time.Now()).First(&b).Error
	return &b, err
}

func (r repository) IsRoomBooked(roomID int64, date time.Time) (bool, error) {
	var count int64
	var booked bool
	err := r.db.Model(&Booking{}).Where("room_id = ? AND booking_time = ?", roomID, date).Count(&count).Error
	if err != nil {
		return booked, err
	}
	
	if count > 0 {
		booked = true
	}
	return booked, nil
}

func InitRepository(db *gorm.DB) Repository {
	return &repository{db}
}
