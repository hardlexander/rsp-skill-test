package booking

import (
	"time"
)

type Booking struct {
	ID           int64     `json:"id" gorm:"type:MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT;primaryKey"`
	RoomID       int64     `json:"room_id" gorm:"type:integer;NOT NULL"`
	GuestID      int64     `json:"guest_id" gorm:"type:integer;NOT NULL"`
	TotalPerson  int64     `json:"total_person" gorm:"type:integer;NOT NULL"`
	BookingTime  time.Time `json:"booking_time" gorm:"type:datetime;NOT NULL"`
	Note         string    `json:"note" gorm:"type:text;NOT NULL"`
	CheckInTime  *time.Time `json:"check_in_time" gorm:"type:datetime"`
	CheckOutTime *time.Time `json:"check_out_time" gorm:"type:datetime"`
	CreatedAt    time.Time `json:"created_at" gorm:"type:timestamp"`
	UpdatedAt    time.Time `json:"updated_at" gorm:"type:timestamp"`
	DeletedAt    *time.Time `json:"deleted_at" gorm:"type:timestamp"`
}
