package authentication

import (
	"errors"
	"time"
	
	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
	
	"RSP-skill-test/pkg/uploader"
	"RSP-skill-test/pkg/util"
)

type Service interface {
	AdminLogin(req AdminLoginRequest) (*LoginResponse, error)
	GuestRegister(req GuestRegisterRequest) error
	GuestLogin(req GuestLoginRequest) (*LoginResponse, error)
	
}

type service struct {
	repo Repository
}

const (
	adminUsername = "admin"
	adminPassword = "password"
	secretSigningKey = "90a7eac7-3383-4c4e-a9b4-431833497631"
)

func (s service) AdminLogin(req AdminLoginRequest) (*LoginResponse, error) {
	isValid := req.Username == adminUsername && req.Password == adminPassword
	if !isValid {
		return nil, errors.New("wrong username/password")
	}
	
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["username"] = "admin"
	claims["role"] = "admin"
	claims["exp"] = time.Now().Add(time.Hour * 24).Unix()
	
	tokenString, err := token.SignedString([]byte(secretSigningKey))
	if err != nil {
		return nil, err
	}
	response := &LoginResponse{Token: tokenString}
	return response, nil
}

func (s service) GuestRegister(req GuestRegisterRequest) error {
	// check if email is registered in db
	emailExists, err := s.repo.CheckIfEmailExists(req.Email)
	if err != nil {
		return err
	}
	
	if emailExists {
		return errors.New("email already registered")
	}
	
	// check if email is a valid email address
	if valid := util.IsValidEmail(req.Email); !valid {
		return errors.New("please input valid email address")
	}
	
	// upload guest profile picture
	var photoURL string
	if req.Photo != nil {
		photoURL, err = uploader.UploadPhoto(req.Photo)
		if err != nil {
			return err
		}
	}
	
	// encrypt password with bcrypt
	encryptedPassword, err := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.MinCost)
	if err != nil {
		return err
	}
	
	g := &Guest{
		Email:     req.Email,
		Password:  encryptedPassword,
		Photo:     photoURL,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
		DeletedAt: nil,
	}
	
	err = s.repo.CreateGuest(g)
	return err
}

func (s service) GuestLogin(req GuestLoginRequest) (*LoginResponse, error) {
	emailExists, err := s.repo.CheckIfEmailExists(req.Email)
	if err != nil {
		return nil, err
	}
	
	if !emailExists {
		return nil, errors.New("email not found")
	}
	
	guest, err := s.repo.GetGuestByEmail(req.Email)
	if err != nil {
		return nil, err
	}
	
	if err := bcrypt.CompareHashAndPassword(guest.Password, []byte(req.Password)); err != nil {
		return nil, err
	}
	
	t := jwt.New(jwt.SigningMethodHS256)
	claims := t.Claims.(jwt.MapClaims)
	claims["username"] = guest.Email
	claims["role"] = "guest"
	claims["id"] = guest.ID
	claims["exp"] = time.Now().Add(time.Hour * 24).Unix()
	
	token, err := t.SignedString([]byte(secretSigningKey))
	response := &LoginResponse{Token: token}
	return response, err
	
}

func InitService(repo Repository) Service {
	return &service{repo}
}