package authentication

import (
	"time"
)

type Guest struct {
	ID        int64     `json:"id" gorm:"type:MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT;primaryKey"`
	Email     string    `json:"email" gorm:"type:varchar(255);NOT NULL"`
	Password  []byte    `json:"password" gorm:"type:varchar(255);NOT NULL"`
	Photo     string    `json:"photo" gorm:"type:varchar(255)"`
	CreatedAt time.Time `json:"created_at" gorm:"type:timestamp"`
	UpdatedAt time.Time `json:"updated_at" gorm:"type:timestamp"`
	DeletedAt *time.Time `json:"deleted_at" gorm:"type:timestamp"`
}
