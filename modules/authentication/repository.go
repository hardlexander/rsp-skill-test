package authentication

import (
	"gorm.io/gorm"
)

type Repository interface {
	CreateGuest(g *Guest) error
	CheckIfEmailExists(email string) (bool, error)
	GetGuestByEmail(email string) (*Guest, error)
	GetGuestByID(id int64) (*Guest, error)
}

type repository struct {
	db *gorm.DB
}

func (r repository) CheckIfEmailExists(email string) (bool, error) {
	var count int64
	var result bool
	err := r.db.Model(&Guest{}).Where("email = ?", email).Count(&count).Error
	if err != nil {
		return result, err
	}
	
	if count > 0 {
		result = true
	}
	
	return result, nil
}

func (r repository) CreateGuest(g *Guest) error {
	err := r.db.Create(g).Error
	return err
}

func (r repository) GetGuestByEmail(email string) (*Guest, error) {
	var g Guest
	err := r.db.Where("deleted_at IS NULL AND email = ?", email).First(&g).Error
	return &g, err
}

func (r repository) GetGuestByID(id int64) (*Guest, error) {
	var g Guest
	err := r.db.Where("deleted_at IS NULL AND id = ?", id).First(&g).Error
	return &g, err
}

func InitRepository(db *gorm.DB) Repository {
	return &repository{db}
}
