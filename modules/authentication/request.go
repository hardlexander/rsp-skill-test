package authentication

import (
	"mime/multipart"
)

type AdminLoginRequest struct {
	Username string
	Password string
}

type GuestRegisterRequest struct {
	Email string
	Password string
	Photo *multipart.FileHeader
}

type GuestLoginRequest struct {
	Email string
	Password string
}
