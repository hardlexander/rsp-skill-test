package room

import (
	"time"
)

type Room struct {
	ID           int64     `json:"id" gorm:"type:MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT;primaryKey"`
	RoomName     string    `json:"room_name" gorm:"type:varchar(255);NOT NULL"`
	RoomCapacity int64     `json:"room_capacity" gorm:"type:integer;NOT NULL"`
	Photo        string    `json:"photo" gorm:"type:varchar(255);NOT NULL"`
	CreatedAt    time.Time `json:"created_at" gorm:"type:timestamp"`
	UpdatedAt    time.Time `json:"updated_at" gorm:"type:timestamp"`
	DeletedAt    *time.Time `json:"deleted_at" gorm:"type:timestamp"`
}
