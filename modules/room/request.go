package room

import (
	"mime/multipart"
)

type CreateRoomRequest struct {
	RoomName     string
	RoomCapacity string
	Photo        *multipart.FileHeader
}

type GetAvailableRoomsRequest struct {
	Date string `json:"date"`
}
