package room

import (
	"strconv"
	"time"
	
	"RSP-skill-test/pkg/uploader"
)

type Service interface {
	CreateRoom(req *CreateRoomRequest) error
	GetAvailableRooms(req *GetAvailableRoomsRequest) (*GetAvailableRoomsResponse, error)
}

type service struct {
	repo Repository
}

const (
	dateLayout = "02-01-2006"
)

func (s service) GetAvailableRooms(req *GetAvailableRoomsRequest) (*GetAvailableRoomsResponse, error) {
	date, err := time.Parse(dateLayout, req.Date)
	if err != nil {
		return nil, err
	}
	
	resp := new(GetAvailableRoomsResponse)
	rooms, err := s.repo.GetAvailableRooms(date)
	if err != nil {
		return nil, err
	}
	
	resp.Rooms = rooms
	return resp, nil
}

func (s service) CreateRoom(req *CreateRoomRequest) error {
	// convert room capacity from string to int64
	roomCapacity, err := strconv.ParseInt(req.RoomCapacity, 10, 64)
	if err != nil {
		return err
	}
	
	// upload photo
	var photoURL string
	if req.Photo != nil {
		photoURL, err = uploader.UploadPhoto(req.Photo)
		if err != nil {
			return err
		}
	}
	
	room := &Room{
		RoomName:     req.RoomName,
		RoomCapacity: roomCapacity,
		Photo:        photoURL,
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
		DeletedAt:    nil,
	}
	
	err = s.repo.CreateRoom(room)
	return err
}

func InitService(repo Repository) Service {
	return &service{repo}
}
