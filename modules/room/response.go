package room

type GetAvailableRoomsResponse struct {
	Rooms []Room `json:"rooms"`
}
