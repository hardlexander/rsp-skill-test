package room

import (
	"time"
	
	"gorm.io/gorm"
)

type Repository interface {
	CreateRoom(room *Room) error
	GetAvailableRooms(date time.Time) ([]Room, error)
	GetRoomCapacity(id int64) (int64, error)
	GetRoomByID(id int64) (*Room, error)
}

type repository struct {
	db *gorm.DB
}

func (r repository) GetAvailableRooms(date time.Time) ([]Room, error) {
	var bookedRoomIDs []int64
	var rooms []Room
	err := r.db.Table("bookings").Select("room_id").Where("booking_time = ?", date).Find(&bookedRoomIDs).Error
	if err != nil {
		return nil, err
	}
	
	err = r.db.Model(&Room{}).Not(bookedRoomIDs).Find(&rooms).Error
	return rooms, err
}

func (r repository) GetRoomCapacity(id int64) (int64, error) {
	var capacity int64
	err := r.db.Model(&Room{}).Select("room_capacity").Where("id = ?", id).First(&capacity).Error
	return capacity, err
}

func (r repository) CreateRoom(room *Room) error {
	err := r.db.Create(room).Error
	return err
}

func (r repository) GetRoomByID(id int64) (*Room, error) {
	var room Room
	err := r.db.Where("deleted_at IS NULL AND id = ?", id).First(&room).Error
	return &room, err
}

func InitRepository(db *gorm.DB) Repository {
	return &repository{db}
}
