module RSP-skill-test

go 1.15

require (
	github.com/99designs/gqlgen v0.13.0
	github.com/aws/aws-sdk-go v1.36.12
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/joho/godotenv v1.3.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0
	github.com/sirupsen/logrus v1.7.0
	github.com/vektah/gqlparser/v2 v2.1.0
	golang.org/x/crypto v0.0.0-20201217014255-9d1352758620
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
	gopkg.in/robfig/cron.v2 v2.0.0-20150107220207-be2e0b0deed5
	gorm.io/driver/mysql v1.0.3
	gorm.io/gorm v1.20.8
)
