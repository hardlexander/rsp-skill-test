package main

import (
	"fmt"
	"time"
	
	"github.com/sirupsen/logrus"
	"gopkg.in/robfig/cron.v2"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	
	"RSP-skill-test/config"
	restHandlers "RSP-skill-test/handlers/rest_handlers"
	"RSP-skill-test/modules/authentication"
	"RSP-skill-test/modules/booking"
	"RSP-skill-test/modules/room"
)

func main() {
	
	errs := make(chan error)
	cfg := config.Get()
	log := logrus.New()
	
	
	// connect to database
	// format dsn = username:password@connection_type(host:port)/db_name?configurations
	dsn := fmt.Sprintf("%s:%s@%s(%s:%s)/%s?%s", cfg.DBUsername, cfg.DBPassword, cfg.DBConnection,
		cfg.DBHost, cfg.DBPort, cfg.DBName, cfg.DBConfigurations)
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
		NowFunc: func() time.Time {
			return time.Now().Local()
		},
	})
	
	if err != nil {
		log.WithError(err).Fatal("REST server failed to connect to database")
		return
	}
	
	// get sql connection from gorm db
	sqlDB, err := db.DB()
	if err != nil {
		log.WithError(err).Fatal("REST server failed to get SQL connection")
		return
	}
	
	// close sql connection when server has stopped
	defer sqlDB.Close()
	
	// start auto migration
	err = db.AutoMigrate(&authentication.Guest{}, &room.Room{}, &booking.Booking{})
	if err != nil {
		log.WithError(err).Fatal("REST server failed to get SQL connection")
		return
	}
	
	// initialize authentication modules
	authRepo := authentication.InitRepository(db)
	authService := authentication.InitService(authRepo)
	authHandler := restHandlers.InitAuthenticationHandler(authService)
	
	// initialize room modules
	roomRepo := room.InitRepository(db)
	roomService := room.InitService(roomRepo)
	roomHandler := restHandlers.InitRoomHandler(roomService)
	
	// initialize booking modules
	bookingRepo := booking.InitRepository(db)
	bookingService := booking.InitService(bookingRepo, roomRepo, authRepo)
	bookingHandler := restHandlers.InitBookingHandler(bookingService)
	
	// initialize router
	router := initRouter(authHandler, roomHandler, bookingHandler)
	
	go func() {
		// start server
		err = router.Start(fmt.Sprintf(":%s", cfg.RestServerPort))
		errs <- err
		close(errs)
	}()
	
	
	go func() {
		// start cron job to send email notification for today's bookings
		c := cron.New()
		c.AddFunc("@daily", func() {
			err = bookingService.NotifyTodayBookings()
			errs <- err
		})
		c.Start()
	}()
	
	
	for {
		select {
		case err = <- errs:
			log.WithError(err).Error("an error happened")
		}
	}
}
