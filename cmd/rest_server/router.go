package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	
	"RSP-skill-test/handlers/rest_handlers"
)

const (
	secretSigningKey = "90a7eac7-3383-4c4e-a9b4-431833497631"
)

func initRouter(authHandler rest_handlers.AuthenticationHandler, roomHandler rest_handlers.RoomHandler,
	bookingHandler rest_handlers.BookingHandler) *echo.Echo {
	router := echo.New()
	router.Use(middleware.Logger(), middleware.Recover())
	
	authAPI := router.Group("/auth")
	authAPI.POST("/login/admin", authHandler.AdminLoginHandler)
	authAPI.POST("/login/guest", authHandler.GuestLoginHandler)
	authAPI.POST("/register/guest", authHandler.GuestRegisterHandler)
	
	roomAPI := router.Group("/room")
	roomAPI.Use(middleware.JWT([]byte(secretSigningKey)))
	roomAPI.POST("/create", roomHandler.CreateRoomHandler)
	roomAPI.GET("/available", roomHandler.GetAvailableRoomsHandler)
	
	bookingAPI := router.Group("/booking")
	bookingAPI.Use(middleware.JWT([]byte(secretSigningKey)))
	bookingAPI.POST("/create", bookingHandler.CreateBookingHandler)
	bookingAPI.POST("/check-in/:id", bookingHandler.CheckInHandler)
	bookingAPI.POST("/check-out/:id", bookingHandler.CheckOutHandler)
	
	return router
	}
