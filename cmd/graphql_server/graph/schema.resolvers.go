package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"RSP-skill-test/cmd/graphql_server/graph/generated"
	"RSP-skill-test/cmd/graphql_server/graph/model"
	"RSP-skill-test/config"
	"RSP-skill-test/handlers/graphql_handlers"
	"RSP-skill-test/modules/authentication"
	"RSP-skill-test/modules/booking"
	"RSP-skill-test/modules/room"
	"context"
	"fmt"
	"strconv"
	"time"

	"github.com/sirupsen/logrus"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func (r *mutationResolver) GuestRegister(ctx context.Context, input model.GuestRegister) (*model.Message, error) {
	msg := new(model.Message)
	err := authHandler.GuestRegisterHandler(ctx, input.Email, input.Password)
	if err != nil {
		msg.Message = err.Error()
		return msg, err
	}
	msg.Message = "success register as a guest"
	return msg, nil
}

func (r *mutationResolver) CreateRoom(ctx context.Context, input model.CreateRoom) (*model.Message, error) {
	req := &room.CreateRoomRequest{
		RoomName:     input.RoomName,
		RoomCapacity: input.RoomCapacity,
	}

	msg := new(model.Message)
	err := roomHandler.CreateRoomHandler(ctx, req)
	if err != nil {
		msg.Message = err.Error()
		return msg, err
	}
	msg.Message = "success to create a room"
	return msg, nil
}

func (r *mutationResolver) CreateBooking(ctx context.Context, input model.CreateBooking) (*model.Message, error) {
	req := &booking.CreateBookingRequest{
		RoomID:      int64(input.RoomID),
		GuestID:     int64(input.GuestID),
		TotalPerson: int64(input.TotalPerson),
		BookingTime: input.BookingTime,
		Note:        input.Note,
	}

	msg := new(model.Message)
	err := bookingHandler.CreateBookingHandler(ctx, req)
	if err != nil {
		msg.Message = err.Error()
		return msg, err
	}
	msg.Message = "success to book a room"
	return msg, nil
}

func (r *mutationResolver) CheckIn(ctx context.Context, bookingID int) (*model.Message, error) {
	msg := new(model.Message)
	err := bookingHandler.CheckInHandler(ctx, int64(bookingID))
	if err != nil {
		msg.Message = err.Error()
		return msg, err
	}
	msg.Message = "success to check in to a room"
	return msg, nil
}

func (r *mutationResolver) CheckOut(ctx context.Context, bookingID int) (*model.Message, error) {
	msg := new(model.Message)
	err := bookingHandler.CheckOutHandler(ctx, int64(bookingID))
	if err != nil {
		msg.Message = err.Error()
		return msg, err
	}
	msg.Message = "success to check out from a room"
	return msg, nil
}

func (r *queryResolver) GetAvailableRooms(ctx context.Context, date string) ([]*model.Room, error) {
	rooms, err := roomHandler.GetAvailableRoomsHandler(ctx, date)
	if err != nil {
		return nil, err
	}

	roomsResponse := make([]*model.Room, len(rooms.Rooms))
	for i, r := range rooms.Rooms {
		rs := &model.Room{
			ID:           strconv.FormatInt(r.ID, 10),
			RoomName:     r.RoomName,
			RoomCapacity: int(r.RoomCapacity),
			Photo:        r.Photo,
		}
		roomsResponse[i] = rs
	}

	return roomsResponse, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }

// !!! WARNING !!!
// The code below was going to be deleted when updating resolvers. It has been copied here so you have
// one last chance to move it out of harms way if you want. There are two reasons this happens:
//  - When renaming or deleting a resolver the old code will be put in here. You can safely delete
//    it when you're done.
//  - You have helper methods in this file. Move them out to keep these resolver files clean.
var roomHandler graphql_handlers.RoomHandler
var authHandler graphql_handlers.AuthenticationHandler
var bookingHandler graphql_handlers.BookingHandler

func init() {
	cfg := config.Get()
	log := logrus.New()
	errs := make(chan error)

	// connect to database
	// format dsn = username:password@connection_tyoe(host:port)/db_name?configurations
	dsn := fmt.Sprintf("%s:%s@%s(%s:%s)/%s?%s", cfg.DBUsername, cfg.DBPassword, cfg.DBConnection,
		cfg.DBHost, cfg.DBPort, cfg.DBName, cfg.DBConfigurations)
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
		NowFunc: func() time.Time {
			return time.Now().Local()
		},
	})

	if err != nil {
		log.WithError(err).Error("GraphQL server failed to connect to database")
		errs <- err
	}

	authRepo := authentication.InitRepository(db)
	authService := authentication.InitService(authRepo)
	authHandler = graphql_handlers.InitAuthenticationHandler(authService)

	roomRepo := room.InitRepository(db)
	roomService := room.InitService(roomRepo)
	roomHandler = graphql_handlers.InitRoomHandler(roomService)

	bookingRepo := booking.InitRepository(db)
	bookingService := booking.InitService(bookingRepo, roomRepo, authRepo)
	bookingHandler = graphql_handlers.InitBookingHandler(bookingService)

}
