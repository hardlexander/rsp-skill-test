package main

import (
	"log"
	"net/http"
	
	"RSP-skill-test/cmd/graphql_server/graph"
	"RSP-skill-test/cmd/graphql_server/graph/generated"
	"RSP-skill-test/config"
	
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
)


func main() {
	
	cfg := config.Get()
	port := cfg.GraphQLServer
	srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{}}))

	http.Handle("/", playground.Handler("GraphQL playground", "/query"))
	http.Handle("/query", srv)

	log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
