FROM golang:1.15-alpine AS build_base

RUN apk add --no-cache git

# Set the Current Working Directory inside the container
WORKDIR /tmp/rsp-skill-test

# We want to populate the module cache based on the go.{mod,sum} files.
COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

# Unit tests
#RUN CGO_ENABLED=0 go test -v

# Build the Go app
#RUN go build -o ./bin/rsp-skill-test .
RUN go build -o ./bin/graphql_server ./cmd/graphql_server

# Start fresh from a smaller image
FROM alpine:3.9
RUN apk add ca-certificates


COPY --from=build_base /tmp/rsp-skill-test/bin/graphql_server /app/graphql_server

# This container exposes port 8080 to the outside world
EXPOSE 90

# Run the binary program produced by `go install`
CMD ["/app/graphql_server"]