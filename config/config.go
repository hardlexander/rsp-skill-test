package config

import (
	"log"
	
	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	DBUsername       string `envconfig:"DB_USERNAME"`
	DBPassword       string `envconfig:"DB_PASSWORD"`
	DBConnection     string `envconfig:"DB_CONNECTION"`
	DBHost           string `envconfig:"DB_HOST"`
	DBPort           string `envconfig:"DB_PORT"`
	DBName           string `envconfig:"DB_NAME"`
	DBConfigurations string `envconfig:"DB_CONFIG" default:"charset=utf8&parseTime=True&loc=Local"`
	
	SMTPHost        string `envconfig:"SMTP_HOST"`
	SMTPPort        int    `envconfig:"SMTP_PORT"`
	EmailSenderName string `envconfig:"EMAIL_SENDER_NAME"`
	AuthEmail       string `envconfig:"AUTH_EMAIL"`
	AuthPassword    string `envconfig:"AUTH_PASSWORD"`
	
	SpaceEndpoint     string `envconfig:"SPACE_ENDPOINT"`
	SpaceRegion       string `envconfig:"SPACE_REGION"`
	SpaceAccessKey    string `envconfig:"SPACE_ACCESS_KEY"`
	SpaceAccessSecret string `envconfig:"SPACE_ACCESS_SECRET"`
	SpaceBucketName   string `envconfig:"SPACE_BUCKET_NAME"`
	
	RestServerPort string `envconfig:"REST_SERVER_PORT"`
	GraphQLServer  string `envconfig:"GRAPHQL_SERVER_PORT"`
}

func Get() Config {
	godotenv.Load(".env")
	cfg := Config{}
	err := envconfig.Process("", &cfg)
	if err != nil {
		log.Fatal(err)
	}
	return cfg
}
