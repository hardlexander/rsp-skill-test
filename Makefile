#!/bin/bash

start-rest:
	@go build -o bin/rest_server ./cmd/rest_server
	@bin/rest_server

start-graphql:
	@go build -o bin/graphql_server ./cmd/graphql_server
	@bin/graphql_server